import React, { useState } from "react";
import { Box, TextField, Button, MenuItem } from "@mui/material";

import { gql, useMutation } from "@apollo/client";

const INTERNAL = "INTERNAL";
const EXTERNAL = "EXTERNAL";

const ABC_BANK = "ABC_BANK";
const XYZ_BANK = "XYZ BANK";

const typeOptions = [
	{
		value: INTERNAL,
		label: "Internal",
	},
	{
		value: EXTERNAL,
		label: "External",
	},
];

const bankOptions = [
	{
		value: ABC_BANK,
		label: "ABC Bank",
	},
	{
		value: XYZ_BANK,
		label: "XYZ Bank",
	},
];

const ADD_TRANSACTION = gql`
	mutation AddTransaction($type: String, $bankName: String, $amount: Float) {
		addTransaction(type: $type, bankName: $bankName, amount: $amount) {
			amount
			bankName
			id
			type
		}
	}
`;

export default function Transaction() {
	const [addTransaction] = useMutation(ADD_TRANSACTION);

	const [type, setType] = useState(INTERNAL);

	const handleSubmit = async (event) => {
		event.preventDefault();
    const formData = new FormData(event.currentTarget);
		const data = {
			accountNumber: formData.get('accountNumber'),
			type,
			bankName: formData.get('bankName'),
			amount: parseFloat(formData.get('amount')),
		}
		await addTransaction({ variables: data});
		window.location = "/dashboard";
	};

	return (
		<div>
			<Box component="form" onSubmit={handleSubmit} noValidate sx={{ mt: 1 }}>
				<TextField
					margin="normal"
					id="outlined-select-currency"
					select
					fullWidth
					label="Type"
					defaultValue={type}
					onChange={(event) => {
						setType(event.target.value);
					}}
				>
					{typeOptions.map((option) => (
						<MenuItem key={option.value} value={option.value}>
							{option.label}
						</MenuItem>
					))}
				</TextField>
				{type === EXTERNAL && (
					<TextField
						margin="normal"
						select
						fullWidth
						label="To Bank"
						id="bankName"
						name="bankName"
					>
						{bankOptions.map((option) => (
							<MenuItem key={option.value} value={option.value}>
								{option.label}
							</MenuItem>
						))}
					</TextField>
				)}

				<TextField
					margin="normal"
					required
					fullWidth
					id="accountNumber"
					label="Account number"
					name="accountNumber"
					autoFocus
				/>
				<TextField
					type="number"
					margin="normal"
					required
					fullWidth
					name="amount"
					label="Amount"
					id="amount"
				/>
				<Button
					type="submit"
					fullWidth
					variant="contained"
					sx={{ mt: 3, mb: 2 }}
				>
					Send
				</Button>
			</Box>
		</div>
	);
}
